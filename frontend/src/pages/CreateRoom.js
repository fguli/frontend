import Counter from "../components/Counter";
import Duration from "../components/Duration";
import classes from "./CreateRoom.module.css";
import React from "react";

function CreateRoom() {
  return (
    <>
      <div className={classes.form}>
        <div className={classes.name}>
          <label>Room name: </label>
          <input type="text" name = "roomname"/>
        </div>
        <div className={classes.counter}>
          <label>Number of players: </label>
          <Counter />
        </div>
        <div className={classes.counter}>
          <label>Duration of game: </label>
          <Duration />
        </div>
        <div className={classes.private}>
          <label>Your room will be: </label>
          <input
            type="checkbox"
            id="is_private"
            name="is_private"
            value="private"
          />
          <h2>Private</h2>
        </div>
        <div className={classes.actions}>
          <button>Submit</button>
        </div>
      </div>
    </>
  );
}

export default CreateRoom;
