import Layout from "./components/layout/Layout";
import EnterGame from "./pages/EnterGame";
import Login from "./pages/Login";
import Register from "./pages/Register";
import QuickGame from "./pages/QuickGame";
import Home from "./pages/Home";
import ChooseRoom from "./pages/ChooseRoom";
import Rules from "./pages/Rules";
import Chat from "./pages/Chat";
import CreateRoom from "./pages/CreateRoom";
import { Route, Routes } from "react-router-dom";
import React from "react";

function App() {
  return (
      <Layout>
          <Routes>
            <Route path="" element={<EnterGame />} />
            <Route path="/login" element={<Login />} />
            <Route path="/register" element={<Register/>} />
            <Route path="/quickGame" element={<QuickGame/>} />
            <Route path="/home" element={<Home/>} />
            <Route path="/rules" element={<Rules />} />
            <Route path="/chooseRoom" element={<ChooseRoom />} />
            <Route path="/createRoom" element={<CreateRoom />} />
            <Route path="/chat" element={<Chat />} />
          </Routes>
      </Layout>
  );
}

export default App;
