import { useState } from "react";
import React from "react";
import classes from "./Counter.module.css";
function Counter() {
  let [num, setNum] = useState(5);
  let incNum = () => {
    if (num < 15) {
      setNum(Number(num) + 1);
    }
  };
  let decNum = () => {
    if (num > 5) {
      setNum(num - 1);
    }
  };
  let handleChange = (e) => {
    setNum(e.target.value);
  };
  return (
    <>
      <div className={classes.counter}>
        <div className={classes.change}>
          <button onClick={decNum}>-</button>
        </div>
        <input type="text" value={num} onChange={handleChange} />
        <div className={classes.change}>
          <button onClick={incNum}>+</button>
        </div>
      </div>
    </>
  );
}

export default Counter;
