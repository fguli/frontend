import { useState } from "react";
import React from "react";
import classes from "./Counter.module.css";
function Duration() {
  let [num, setNum] = useState(0);
  let incNum = () => {
    if (num < 60) {
      setNum(Number(num) + 5);
    }
  };
  let decNum = () => {
    if (num > 0) {
      setNum(num - 5);
    }
  };
  let handleChange = (e) => {
    setNum(e.target.value);
  };
  return (
    <>
      <div className={classes.counter}>
        <div className={classes.change}>
          <button onClick={decNum}>-</button>
        </div>
        <input type="text" value={num} onChange={handleChange} />
        <div className={classes.change}>
          <button onClick={incNum}>+</button>
        </div>
      </div>
    </>
  );
}

export default Duration;
